﻿using System;

namespace module5._0
{
    class Program
    {
        static void Main(string[] args)
        {
            Program prog = new Program();
            int x = 9, y = 0, health = 10;
            int[,] bombs = new int[10, 10];
            prog.Bombs(bombs);
            try
            {
                while (x != -1)
                {
                    prog.View(prog.Field(x, y), health);
                    prog.Сontrol(ref x, ref y);
                    prog.Boom(bombs, ref health, x, y);
                    Console.Clear();
                    if (prog.Congratulations(ref x, ref y, ref health)) break;
                }
            }
            catch
            {
                throw new ArgumentNullException("Field", "cannot be displayed");
            }
        }

        public char[,] Field(int x, int y)
        {
            char[,] field = new char[10, 10];
            for (int i = 0; i < 10; i++)
                for (int j = 0; j < 10; j++)
                    field[i, j] = '.';
            field[x, y] = '*';
            field[0, 9] = 'П';
            return field;
        }

        public bool Congratulations(ref int x, ref int y, ref int health)
        {
            Program prog = new Program();
            if (health <= 0)
            {
                Console.WriteLine("You lose!");
                health = 10;
                return prog.Contenued(ref x, ref y);
            }
            else if (x == 0 && y == 9)
            {
                Console.WriteLine("\nП: Ты спас меня!\n\nYou win!!!\nYou win!!!\nYou win!!!");
                Console.WriteLine();
                health = 10;
                return prog.Contenued(ref x, ref y);
            }
            else return false;
        }

        public bool Contenued(ref int x, ref int y)
        {
            Console.WriteLine("Хотите сыграть ещё? (Enter - ok, Esc - no)");
            if (Console.ReadKey().Key == ConsoleKey.Enter)
            {
                x = 9;
                y = 0;
                Console.Clear();
                return false;
            }
            else if (Console.ReadKey().Key == ConsoleKey.Escape) return true;
            else return Contenued(ref x, ref y);


        }

        public void Сontrol(ref int x, ref int y)
        {
            Program prog = new Program();
            switch (Console.ReadKey(true).Key) 
            {
                case ConsoleKey.W: --x; prog.Borders(ref x, ref y); break;
                case ConsoleKey.S: ++x; prog.Borders(ref x, ref y); break;
                case ConsoleKey.A: --y; prog.Borders(ref x, ref y); break;
                case ConsoleKey.D: ++y; prog.Borders(ref x, ref y); break;
            }
        }

        public void Borders(ref int x, ref int y)
        {
            if (x == -1) ++x;
            if (x == 10) --x;
            if (y == -1) ++y;
            else if (y == 10) --y;
        }

        public void Bombs(int[,] field)
        {
            Random rnd = new Random();
            for (int i = 0; i < 10; i++)
            {
                int x = rnd.Next(0, 100);
                int b = rnd.Next(1, 10);
                if (((x - 11) / 10 == 9 && (x - 11) % 10 == 0) || ((x - 11) / 10 == 0 && (x - 11) % 10 == 9));
                else
                {
                    if (field[Math.Abs((x - 11) / 10), Math.Abs((x - 11) % 10)] == 0)
                        field[Math.Abs((x - 11) / 10), Math.Abs((x - 11) % 10)] = b;
                }
            }
        }

        public void View(char[,] field, int health)
        {
            Console.WriteLine("П: Спасите! Please\n");
            Console.WriteLine("Доберитесь до Принцессы (П)\nPS. ОСТОРОЖНО! Мины!");
            Console.WriteLine("W - вперёд, S - назад, A - влево, D - вправо");
            Console.WriteLine();
            Console.Write("Здоровье: {0}", health);
            Console.WriteLine();
            Console.WriteLine();
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                    Console.Write(field[i, j]);
                Console.WriteLine();
            }
        }

        public void Boom(int [,] bombs, ref int health, int x, int y)
        {
            if (bombs[x, y] > 0)
            {
                health -= bombs[x, y];
                bombs[x, y] = 0;
                Console.Clear();
                Console.WriteLine("BOOM!\nBOOM!\nBOOM!\nBOOM!\nBOOM!\nBOOM!\nBOOM!");
                Console.ReadKey();
            }
        }

        
    }
}
